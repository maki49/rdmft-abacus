#include "rdmft_test.h"

#include "module_base/blas_connector.h"
#include "module_base/scalapack_connector.h"
#include "module_base/timer.h"
#include "module_psi/psi.h"

#include "module_hamilt_lcao/hamilt_lcaodft/LCAO_matrix.h"
// #include "module_elecstate/module_dm/density_matrix.h"
#include "module_elecstate/module_dm/cal_dm_psi.h"
#include <cmath>

#include <iostream>
#include <fstream>
#include <sstream>

#include <complex>

// for test use dgemm_
#include "module_base/matrix.h"
#include "module_base/blas_connector.h"

namespace hamilt
{
    // wfc and H_wfc need to be k_firest and provide wfc(ik, 0, 0) and H_wfc(ik, 0, 0)
    // psi::Psi<TK> psi's (), std::vector<TK> HK's [] operator overloading return TK
    template <>
    void HkPsi<std::complex<double>>(const Parallel_Orbitals* ParaV, const Parallel_2D& para_wfc_in, const std::complex<double>& HK, const std::complex<double>& wfc, std::complex<double>& H_wfc)
    {

        const int one_int = 1;
        //const double one_double = 1.0, zero_double = 0.0;
        const std::complex<double> one_complex = { 1.0, 0.0 };
        const std::complex<double> zero_complex = { 0.0, 0.0 };
        const char N_char = 'N';
        const char T_char = 'T';

        const int nbasis = ParaV->desc[2];
        const int nbands = ParaV->desc_wfc[3];

        //test
        std::cout << "\n\n\n******\nH_wfc[0], Hwfc[1]: " << H_wfc << " " << *(&H_wfc + 1) << "\n";

        //because wfc(bands, basis'), H(basis, basis'), we do wfc*H^T(in the perspective of cpp, not in fortran). And get H_wfc(bands, basis) is correct.
        // pzgemm_( &N_char, &T_char, &nbands, &nbasis, &nbasis, &one_complex, wfc, &one_int, &one_int, ParaV->desc_wfc,
        //     HK, &one_int, &one_int, ParaV->desc, &zero_complex, H_wfc, &one_int, &one_int, ParaV->desc_wfc );
        // pzgemm_( &T_char, &N_char, &nbasis, &nbands, &nbasis, &one_complex, &HK, &one_int, &one_int, ParaV->desc,
        //     &wfc, &one_int, &one_int, ParaV->desc_wfc, &zero_complex, &H_wfc, &one_int, &one_int, ParaV->desc_wfc );

        // zgemm_( &T_char, &N_char, &nbasis, &nbands, &nbasis, &one_complex, &HK, &nbasis, &wfc, &nbasis, &zero_complex, &H_wfc, &nbasis );

        for (int ib = 0; ib < nbands; ++ib)
        {
            for (int ibs2 = 0; ibs2 < nbasis; ++ibs2)
            {
                for (int ibs1 = 0; ibs1 < nbasis; ++ibs1)
                {
                    *(&H_wfc + ib * nbasis + ibs2) += *(&HK + ibs2 * nbasis + ibs1) * *(&wfc + ib * nbasis + ibs1);
                }
            }
        }




        //test
        std::cout << "after HkPsi(),\nH_wfc[0], Hwfc[1]: " << H_wfc << " " << *(&H_wfc + 1) << "\n******\n\n\n";


        /*
        if ( std::is_same<TK, double>::value )
        {
            pdgemm_( &N_char, &T_char, &nbands, &nbasis, &nbasis, &one_double, &wfc, &one_int, &one_int, ParaV->desc_wfc,
                    &HK, &one_int, &one_int, ParaV->desc, &zero_double, &H_wfc, &one_int, &one_int, ParaV->desc_wfc );
        }
        else if ( std::is_same<TK, std::complex<double>>::value )
        {
            pzgemm_( &N_char, &T_char, &nbands, &nbasis, &nbasis, &one_complex, &wfc, &one_int, &one_int, ParaV->desc_wfc,
                    &HK, &one_int, &one_int, ParaV->desc, &zero_complex, &H_wfc, &one_int, &one_int, ParaV->desc_wfc );
        }
        else std::cout << "\n\n******\nthere maybe something wrong when calling rdmft_cal() and use pdemm_/pzgemm_\n******\n\n";
        */
    }

    template <>
    void HkPsi<double>(const Parallel_Orbitals* ParaV, const Parallel_2D& para_wfc_in, const double& HK, const double& wfc, double& H_wfc)
    {
        const int one_int = 1;
        const double one_double = 1.0;
        const double zero_double = 0.0;
        const char N_char = 'N';
        const char T_char = 'T';

        const int nbasis = ParaV->desc[2];
        const int nbands = ParaV->desc_wfc[3];

        //because wfc(bands, basis'), H(basis, basis'), we do wfc*H^T(in the perspective of cpp, not in fortran). And get H_wfc(bands, basis) is correct.
        // pdgemm_( &T_char, &N_char, &nbasis, &nbands, &nbasis, &one_double, &HK, &one_int, &one_int, ParaV->desc,
        //     &wfc, &one_int, &one_int, ParaV->desc_wfc, &zero_double, &H_wfc, &one_int, &one_int, ParaV->desc_wfc );
        dgemm_(&T_char, &N_char, &nbasis, &nbands, &nbasis, &one_double, &HK, &nbasis, &wfc, &nbasis, &zero_double, &H_wfc, &nbasis);

    }
    // ModuleBase::matrix wfcHwfc(ik, 0)
    template <>
    void psiDotPsi<std::complex<double>>(const Parallel_Orbitals* ParaV, const Parallel_2D& para_wfc_in, const Parallel_2D& para_Eij_in,
        const std::complex<double>& wfc, const std::complex<double>& H_wfc, std::vector<std::complex<double>>& Dmn, double* wfcHwfc)
    {
        const int one_int = 1;
        //const double one_double = 1.0, zero_double = 0.0;
        const std::complex<double> one_complex = { 1.0, 0.0 };
        const std::complex<double> zero_complex = { 0.0, 0.0 };
        const char N_char = 'N';
        const char C_char = 'C';

        const int nbasis = ParaV->desc[2];
        const int nbands = ParaV->desc_wfc[3];

        // const int nrow_bands = para_Eij_in.get_row_size();
        // const int ncol_bands = para_Eij_in.get_col_size();
        const int nrow_bands = para_Eij_in.get_row_size();
        const int ncol_bands = para_Eij_in.get_col_size();

        //test
        std::cout << "\n\n\n******\nwfcHwfc[0], wfcHwfc[1]: " << wfcHwfc[0] << " " << wfcHwfc[1] << "\n";

        // in parallel_orbitals.h, there has int desc_Eij[9] which used for Eij in TDDFT, nbands*nbands. Just proper here.
        // std::vector<TK> Dmn(nrow_bands*ncol_bands);
        // pzgemm_( &C_char, &N_char, &nbands, &nbands, &nbasis, &one_complex, &wfc, &one_int, &one_int, ParaV->desc_wfc,
        //         &H_wfc, &one_int, &one_int, ParaV->desc_wfc, &zero_complex, &Dmn[0], &one_int, &one_int, para_Eij_in.desc );
        // zgemm_( &C_char, &N_char, &nbands, &nbands, &nbasis, &one_complex,  &wfc, &nbasis, &H_wfc, &nbasis, &zero_complex, &Dmn[0], &nbands );

        set_zero_HK(Dmn);
        for (int ib1 = 0; ib1 < nbands; ++ib1)
        {
            for (int ib2 = 0; ib2 < nbands; ++ib2)
            {
                for (int ibas = 0; ibas < nbasis; ++ibas)
                {
                    Dmn[ib1 * nbands + ib2] += std::conj(*(&wfc + ib1 * nbasis + ibas)) * *(&H_wfc + ib2 * nbasis + ibas);
                }
            }
        }



        for (int i = 0; i < nrow_bands; ++i)
        {
            int i_global = para_Eij_in.local2global_row(i);
            for (int j = 0; j < ncol_bands; ++j)
            {
                int j_global = para_Eij_in.local2global_col(j);
                if (i_global == j_global)
                {
                    wfcHwfc[j_global] = std::real(Dmn[i * ncol_bands + j]); // need to be sure imag(Dmn[i*nrow_bands+j]) == 0
                    // double testEnn = std::abs( std::imag( Dmn[i*ncol_bands+j] ) );
                    // if( testEnn>1e-16 )std::cout << "\n\nimag(Enn)!=0? imag(Enn)= " << testEnn << "\n\n";
                }
            }
        }


    }

    template <>
    void psiDotPsi<double>(const Parallel_Orbitals* ParaV, const Parallel_2D& para_wfc_in, const Parallel_2D& para_Eij_in,
        const double& wfc, const double& H_wfc, std::vector<double>& Dmn, double* wfcHwfc)
    {
        const int one_int = 1;
        const double one_double = 1.0;
        const double zero_double = 0.0;
        const char N_char = 'N';
        const char T_char = 'T';

        const int nbasis = ParaV->desc[2];
        const int nbands = ParaV->desc_wfc[3];

        // const int nrow_bands = ParaV->nrow_bands;
        // const int ncol_bands = ParaV->ncol_bands;
        const int nrow_bands = para_Eij_in.get_row_size();
        const int ncol_bands = para_Eij_in.get_col_size();

        // in parallel_orbitals.h, there has int desc_Eij[9] which used for Eij in TDDFT, nbands*nbands. Just proper here.
        // std::vector<double> Dmn(ncol_bands*nrow_bands); /////////////////////////////////////////////////////////////////////////////////////////////////
        // pdgemm_( &T_char, &N_char, &nbands, &nbands, &nbasis, &one_double, &wfc, &one_int, &one_int, ParaV->desc_wfc,
        //         &H_wfc, &one_int, &one_int, ParaV->desc_wfc, &zero_double, &Dmn[0], &one_int, &one_int, para_Eij_in.desc );

        dgemm_(&T_char, &N_char, &nbands, &nbands, &nbasis, &one_double, &wfc, &nbasis, &H_wfc, &nbasis, &zero_double, &Dmn[0], &nbands);

        for (int i = 0; i < nrow_bands; ++i)
        {
            int i_global = para_Eij_in.local2global_row(i);
            for (int j = 0; j < ncol_bands; ++j)
            {
                int j_global = para_Eij_in.local2global_col(j);
                if (i_global == j_global)
                {
                    wfcHwfc[j_global] = std::real(Dmn[i * ncol_bands + j]); // need to be sure imag(Dmn[i*ncol_bands+j]) == 0
                    // double testEnn = std::abs( std::imag( Dmn[i*ncol_bands+j] ) );
                    // if( testEnn>1e-16 )std::cout << "\n\nimag(Enn)!=0? imag(Enn)= " << testEnn << "\n\n";
                }
            }
        }
    }


// for test use dgemm_
void printResult_dgemm()
{
    std::vector<double> MA = {1.1, 1.2, 3.0, 2.0, 2.1, 4.3, 3.5, 3.6, 6.0};
    std::vector<double> MB = {0.7, 0.6, 1.0, 1.1, 2.0, 3.1};
    std::vector<double> MC(3*2);

    const int one_int = 1;
    const double one_double = 1.0;
    const double zero_double = 0.0;
    const char N_char = 'N';
    const char T_char = 'T';

    int M_three = 3;
    int N_two = 2;
    int K_three = 3;

    MC[0] = 1.1;

    dgemm_(&N_char, &N_char, &N_two, &M_three, &K_three, &one_double, &MB[0], &N_two, &MA[0], &M_three, &zero_double, &MC[0], &N_two);
    std::cout << "\n\n\n******\n";
    printMatrix_vector(M_three, M_three, MA, "matrixA");
    printMatrix_vector(M_three, N_two, MB, "matrixB");
    printMatrix_vector(M_three, N_two, MC, "matrixC");
    std::cout << "\n******\n\n\n";

}


// for test add a function and call it in source/module_esolver/esolver_ks_lcao.cpp, and in (ModuleESolver::) ESolver_KS_LCAO<TK, TR>::afterscf() function

// wg_wfcHwfc = wg*wfcHwfc + wg_wfcHwfc
//  Default symbol=0. When symbol = 0, 1, 2, 3, 4, wg = wg, 0.5*wg, g(wg), 0.5*g(wg), d_g(wg)/d_ewg respectively.
void wgMul_wfcHwfc(const ModuleBase::matrix& wg, const ModuleBase::matrix& wfcHwfc, ModuleBase::matrix& wg_wfcHwfc, int symbol)
{
    for(int ir=0; ir<wg.nr; ++ ir)
    {
        for(int ic=0; ic<wg.nc; ++ic) wg_wfcHwfc(ir, ic) += wg_func(wg(ir, ic), symbol) * wfcHwfc(ir, ic);
    } 
}


// Default symbol = 0 for the gradient of Etotal with respect to occupancy
// symbol = 1 for the relevant calculation of Etotal
void add_wg(const ModuleBase::matrix& wg, const ModuleBase::matrix& wfcHwfc_TV_in, const ModuleBase::matrix& wfcHwfc_hartree_in,
                const ModuleBase::matrix& wfcHwfc_XC_in, ModuleBase::matrix& wg_wfcHwfc, int symbol)
{
    
    wg_wfcHwfc.zero_out();
    
    if( symbol==0 )
    {
        wgMul_wfcHwfc(wg, wfcHwfc_XC_in, wg_wfcHwfc, 4);
        wg_wfcHwfc+=(wfcHwfc_TV_in);
        wg_wfcHwfc+=(wfcHwfc_hartree_in);
    }
    else if( symbol==1 )
    {
        wgMul_wfcHwfc(wg, wfcHwfc_TV_in, wg_wfcHwfc);
        wgMul_wfcHwfc(wg, wfcHwfc_hartree_in, wg_wfcHwfc, 1);
        wgMul_wfcHwfc(wg, wfcHwfc_XC_in, wg_wfcHwfc, 3);
    }
    else std::cout << "\n\n\n******\nthere are something wrong when calling rdmft_test() and calculation add_wd()\n******\n\n\n"; 
}


//give certain wg_wfcHwfc, get the corresponding energy
double sumWg_getEnergy(const ModuleBase::matrix& wg_wfcHwfc)
{
    double energy = 0.0;
    for(int ir=0; ir<wg_wfcHwfc.nr; ++ ir)
    {
        for(int ic=0; ic<wg_wfcHwfc.nc; ++ic) energy += wg_wfcHwfc(ir, ic);
    }
    return energy;
}


// return the function of eta, g(eta)=eta
// When symbol = 0, 1, 2, 3, 4, 5, return eta, 0.5*eta, g(eta), 0.5*g(eta), d_g(eta)/d_eta, 1.0 respectively. Default symbol=0.
double wg_func(double eta, int symbol)
{
    if( symbol==0 ) return eta ;
    else if ( symbol==1 ) return 0.5*eta ;
    else if ( symbol==2 ) return eta;               //return std::pow(eta, 2.0) * 1.0 ;
    else if ( symbol==3 ) return 0.5*eta;           //return 0.5*std::pow(eta, 2.0) * 1.0 ;
    else if ( symbol==4 ) return 1.0;
    else 
    {
        std::cout << "\n!!!!!!\nThere may be some errors when calling wgMulPsi function\n!!!!!!\n";
        return eta ;
    }
}




}


