#ifndef RDMFT_TEST_H
#define RDMFT_TEST_H

#include "module_base/matrix.h"
//#include "module_elecstate/module_dm/density_matrix.h"
#include "module_hamilt_lcao/hamilt_lcaodft/LCAO_matrix.h"
#include "module_basis/module_ao/parallel_orbitals.h"
#include "module_cell/module_neighbor/sltk_grid_driver.h"
#include "module_cell/unitcell.h"
#include "module_hamilt_lcao/module_gint/gint_gamma.h"
#include "module_hamilt_lcao/module_gint/gint_k.h"
#include "module_elecstate/potentials/potential_new.h"
#include "module_base/blas_connector.h"
#include "module_base/scalapack_connector.h"
#include "module_base/blas_connector.h"
#include "module_basis/module_ao/parallel_2d.h"
#include "module_basis/module_ao/parallel_orbitals.h"
#include "module_hamilt_pw/hamilt_pwdft/global.h"

#include "module_hamilt_general/operator.h"
#include "module_hamilt_lcao/module_hcontainer/hcontainer.h"
#include "module_hamilt_lcao/hamilt_lcaodft/operator_lcao/operator_lcao.h"
#include "module_hamilt_lcao/hamilt_lcaodft/operator_lcao/op_exx_lcao.h"
#include "module_hamilt_lcao/hamilt_lcaodft/operator_lcao/ekinetic_new.h"
#include "module_hamilt_lcao/hamilt_lcaodft/operator_lcao/nonlocal_new.h"
#include "module_hamilt_lcao/hamilt_lcaodft/operator_lcao/veff_lcao.h"

#include <iostream>
#include <type_traits>
#include <complex>



namespace hamilt
{

// for test use dgemm_
void printResult_dgemm();

//for print matrix
template <typename TK>
void printMatrix_pointer(int M, int N, TK& matrixA, std::string nameA);

template <typename TK>
void printMatrix_vector(int M, int N, std::vector<TK>& matrixA, std::string nameA);

double wg_func(double wg, int symbol = 0);


template <typename TK>
void set_zero_HK(std::vector<TK>& HK);

// wfc and H_wfc need to be k_firest and provide wfc(ik, 0, 0) and H_wfc(ik, 0, 0)
// psi::Psi<TK> psi's (), std::vector<TK> HK's [] operator overloading return TK
template <typename TK>
void HkPsi(const Parallel_Orbitals* ParaV, const Parallel_2D& para_wfc_in, const TK& HK, const TK& wfc, TK& H_wfc);

// template <>
// void HkPsi<double>(const Parallel_Orbitals* ParaV, const Parallel_2D& para_wfc_in, const double& HK, const double& wfc, double& H_wfc);

// ModuleBase::matrix wfcHwfc(ik, 0)
template <typename TK>
void psiDotPsi(const Parallel_Orbitals* ParaV, const Parallel_2D& para_wfc_in, const Parallel_2D& para_Eij_in,
    const TK& wfc, const TK& H_wfc, std::vector<TK>& Dmn, double* wfcHwfc);

// template <>
// void psiDotPsi<double>(const Parallel_Orbitals* ParaV, const Parallel_2D& para_wfc_in, const Parallel_2D& para_Eij_in,
//     const double& wfc, const double& H_wfc, std::vector<double>& Dmn, double* wfcHwfc);

// realize wg_wfc = wg * wfc. Calling this function and we can get wfc = wg*wfc.
template <typename TK>
void wgMulPsi(const Parallel_Orbitals* ParaV, const Parallel_2D& para_wfc_in, const ModuleBase::matrix& wg, psi::Psi<TK>& wfc, int symbol = 0);

// add psi with eta and g(eta)
template <typename TK>
void add_psi(const Parallel_Orbitals* ParaV, const Parallel_2D& para_wfc_in, const ModuleBase::matrix& wg,
    psi::Psi<TK>& psi_TV, psi::Psi<TK>& psi_hartree, psi::Psi<TK>& psi_XC, psi::Psi<TK>& wg_Hpsi);

// wg_wfcHwfc = wg*wfcHwfc + wg_wfcHwfc
// When symbol = 0, 1, 2, 3, 4, wg = wg, 0.5*wg, g(wg), 0.5*g(wg), d_g(wg)/d_ewg respectively. Default symbol=0.
void wgMul_wfcHwfc(const ModuleBase::matrix& wg, const ModuleBase::matrix& wfcHwfc, ModuleBase::matrix& wg_wfcHwfc, int symbol = 0);


// Default symbol = 0 for the gradient of Etotal with respect to occupancy
// symbol = 1 for the relevant calculation of Etotal
void add_wg(const ModuleBase::matrix& wg, const ModuleBase::matrix& wfcHwfc_TV_in, const ModuleBase::matrix& wfcHwfc_hartree_in,
                const ModuleBase::matrix& wfcHwfc_XC_in, ModuleBase::matrix& wg_wfcHwfc, int symbol = 0);


//give certain wg_wfcHwfc, get the corresponding energy
double sumWg_getEnergy(const ModuleBase::matrix& wg_wfcHwfc);


// for test add a function and call it in module_elecstate/elecstate_lcao.cpp
// !!!just used for k-dependent grid integration. For gamma only algorithms, transfer Gint_k& GK_in to Gint_Gamma GG_in and use it in Veff<OperatorLCAO<TK, TR>>
template <typename TK, typename TR>
double rdmft_cal(LCAO_Matrix* LM_in,
    Parallel_Orbitals* ParaV,
    const ModuleBase::matrix& wg,
    const psi::Psi<TK>& wfc,
    ModuleBase::matrix& wg_wfcHamiltWfc,
    psi::Psi<TK>& wg_HamiltWfc,
    const K_Vectors& kv_in,
    Gint_k& GK_in,
    Local_Orbital_Charge& loc_in,
    elecstate::Potential& pot_in);

}

#include "rdmft_test.hpp"

#endif
